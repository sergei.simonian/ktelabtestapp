package com.example.healthcare.rest.controller;

import com.example.healthcare.entity.Ticket;
import com.example.healthcare.rest.dto.AppointmentRequestDto;
import com.example.healthcare.rest.dto.TicketInfoRequestDto;
import com.example.healthcare.rest.dto.TicketInfoResponseDto;
import com.example.healthcare.rest.dto.TicketResponseDto;
import com.example.healthcare.service.TicketService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;

import java.sql.Date;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.List;

@RestController
@RequiredArgsConstructor
public class TicketController implements HealthcareApi {

    private final TicketService ticketService;

    @Override
    public ResponseEntity<TicketResponseDto> getAppointmentWithDoctor(AppointmentRequestDto body) {
        Ticket ticketAppointment =
                ticketService.getAppointmentWithDoctor(body.getDoctor(), body.getPatient(), body.getTicket());

        TicketResponseDto ticketResponseDto = new TicketResponseDto();
        ticketResponseDto.setDoctor(ticketAppointment.getDoctor().getId());
        ticketResponseDto.setPatient(ticketAppointment.getPatient().getId());
        ticketResponseDto.setDate(
                convertToDateViaInstant(
                        LocalDateTime.of(ticketAppointment.getDate().toLocalDate(),
                                ticketAppointment.getBeginTime().toLocalTime())));
        return ResponseEntity.ok(ticketResponseDto);
    }

    @Override
    public ResponseEntity<List<TicketInfoResponseDto>> getFreeTickets(TicketInfoRequestDto body) {
        Long doctorId = body.getDoctor();
        java.util.Date dateInput = body.getDate();
        LocalDate localDate = LocalDate.ofInstant(dateInput.toInstant(), ZoneId.systemDefault());
        Date date = Date.valueOf(localDate);
        List<Ticket> tickets = ticketService.getFreeTickets(doctorId, date);


        List<TicketInfoResponseDto> ticketDtos = new ArrayList<>();
        for (Ticket ticket : tickets) {
            TicketInfoResponseDto ticketDto = new TicketInfoResponseDto();
            LocalDateTime localDateTime =
                    LocalDateTime.of(ticket.getDate().toLocalDate(), ticket.getBeginTime().toLocalTime());

            ticketDto.setDatetime(convertToDateViaInstant(localDateTime));
            ticketDto.setDoctor(ticket.getDoctor().getId());
            ticketDtos.add(ticketDto);
        }
        return ResponseEntity.ok(ticketDtos);
    }

    @Override
    public ResponseEntity<List<TicketResponseDto>> ticketsById(Long patientId) {
        List<Ticket> tickets = ticketService.getAllPatientTickets(patientId);
        List<TicketResponseDto> ticketDtos = new ArrayList<>();


        for (Ticket ticket : tickets) {
            TicketResponseDto ticketDto = new TicketResponseDto();
            LocalDateTime localDateTime =
                    LocalDateTime.of(ticket.getDate().toLocalDate(), ticket.getBeginTime().toLocalTime());
            ticketDto.setDate(convertToDateViaInstant(localDateTime));
            ticketDto.setDoctor(ticket.getDoctor().getId());
            ticketDto.setPatient(ticket.getPatient().getId());
            ticketDtos.add(ticketDto);
        }
        return ResponseEntity.ok(ticketDtos);
    }

    java.util.Date convertToDateViaInstant(LocalDateTime dateToConvert) {
        return java.util.Date
                .from(dateToConvert.atZone(ZoneId.of("+00:00:00"))
                        .toInstant());
    }
}
