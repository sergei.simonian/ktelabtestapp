package com.example.healthcare.service;

import com.example.healthcare.entity.Ticket;

import java.sql.Date;
import java.util.List;

public interface TicketService {

    List<Ticket> getFreeTickets(Long doctorId, Date date);

    Ticket getAppointmentWithDoctor(Long doctor, Long patient, Long ticket);

    List<Ticket> getAllPatientTickets(Long patientId);
}
