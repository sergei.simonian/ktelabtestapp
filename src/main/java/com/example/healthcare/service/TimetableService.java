package com.example.healthcare.service;

import com.example.healthcare.entity.Ticket;
import io.spring.testapp.AddTimetableRequest;

import java.util.List;

public interface TimetableService {
    List<Ticket> generateAndSaveTickets(AddTimetableRequest addTimetableRequest) throws Exception;

}
