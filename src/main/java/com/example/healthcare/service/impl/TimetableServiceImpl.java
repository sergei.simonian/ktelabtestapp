package com.example.healthcare.service.impl;

import com.example.healthcare.entity.Doctor;
import com.example.healthcare.entity.Ticket;
import com.example.healthcare.exception.DataNotFoundException;
import com.example.healthcare.repository.DoctorRepository;
import com.example.healthcare.repository.TicketRepository;
import com.example.healthcare.soap.adapter.DurationAdapter;
import com.example.healthcare.service.TimetableService;
import io.spring.testapp.AddTimetableRequest;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Transactional;

import java.sql.Date;
import java.sql.Time;
import java.time.Duration;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class TimetableServiceImpl implements TimetableService {

    private final TicketRepository ticketRepository;
    private final DoctorRepository doctorRepository;
    private final DurationAdapter durationAdapter;

    @Override
    @Transactional(isolation = Isolation.REPEATABLE_READ)
    public List<Ticket> generateAndSaveTickets(AddTimetableRequest addTimetableRequest) throws Exception {
        Doctor doctor = doctorRepository.findById(addTimetableRequest.getDoctorId())
                .orElseThrow(() -> new DataNotFoundException("doctor not found"));

        LocalTime beginTime = addTimetableRequest.getBeginTime().toGregorianCalendar().toZonedDateTime().toLocalTime();
        LocalTime endTime = addTimetableRequest.getEndTime().toGregorianCalendar().toZonedDateTime().toLocalTime();
        Duration ticketDuration = durationAdapter.unmarshal(addTimetableRequest.getTicketDuration());

        List<Ticket> tickets = new ArrayList<>();

        while (endTime.isAfter(beginTime)) {
            Ticket ticket = Ticket.builder()
                    .doctor(doctor)
                    .date(Date.valueOf(addTimetableRequest.getDate().toGregorianCalendar().toZonedDateTime().toLocalDate()))
                    .beginTime(Time.valueOf(beginTime))
                    .endTime(Time.valueOf(beginTime.plus(ticketDuration)))
                    .available(true)
                    .build();
            beginTime = beginTime.plus(ticketDuration);
            tickets.add(ticket);
        }
        return ticketRepository.saveAll(tickets);
    }

}
