package com.example.healthcare.service.impl;

import com.example.healthcare.entity.Doctor;
import com.example.healthcare.entity.Patient;
import com.example.healthcare.entity.Ticket;
import com.example.healthcare.exception.DataNotFoundException;
import com.example.healthcare.repository.DoctorRepository;
import com.example.healthcare.repository.PatientRepository;
import com.example.healthcare.repository.TicketRepository;
import com.example.healthcare.service.TicketService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.sql.Date;
import java.util.List;

@Service
@RequiredArgsConstructor
public class TicketServiceImpl implements TicketService {

    private final TicketRepository ticketRepository;
    private final PatientRepository patientRepository;
    private final DoctorRepository doctorRepository;

    @Override
    @Transactional(readOnly = true)
    public List<Ticket> getFreeTickets(Long doctorId, Date date) {
        boolean available = true;
        return ticketRepository.findAllByDateAndDoctor_IdAndAvailable(date, doctorId, available);
    }

    @Override
    @Transactional
    public Ticket getAppointmentWithDoctor(Long doctorId, Long patientId, Long ticketId) {
        Ticket ticket = ticketRepository.findById(ticketId)
                .orElseThrow(() -> new DataNotFoundException("Ticket not found"));

        Patient patient = patientRepository.findById(patientId)
                .orElseThrow(() -> new DataNotFoundException("Patient not found"));

        Doctor doctor = doctorRepository.findById(doctorId)
                .orElseThrow(() -> new DataNotFoundException("Doctor not found"));

        ticket.setPatient(patient);
        ticket.setDoctor(doctor);
        ticket.setAvailable(false);
        return ticketRepository.save(ticket);
    }

    @Override
    public List<Ticket> getAllPatientTickets(Long patientId) {
        return ticketRepository.findAllByPatient_Id(patientId);
    }

}
