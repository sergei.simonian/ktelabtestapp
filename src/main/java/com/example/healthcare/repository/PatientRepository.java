package com.example.healthcare.repository;


import com.example.healthcare.entity.Patient;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Repository for working with patients
 */
public interface PatientRepository extends JpaRepository<Patient, Long> {

}
