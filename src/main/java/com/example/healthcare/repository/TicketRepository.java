package com.example.healthcare.repository;


import com.example.healthcare.entity.Ticket;
import org.springframework.data.jpa.repository.JpaRepository;

import java.sql.Date;
import java.util.List;

/**
 * Repository for working with tickets
 */
public interface TicketRepository extends JpaRepository<Ticket, Long> {

    List<Ticket> findAllByDateAndDoctor_IdAndAvailable(Date date, Long doctorId, Boolean available);

    List<Ticket> findAllByPatient_Id(Long patientId);
}
