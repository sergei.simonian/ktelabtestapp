package com.example.healthcare.repository;


import com.example.healthcare.entity.Doctor;
import org.springframework.data.jpa.repository.JpaRepository;


/**
 * Repository for working with doctors
 */
public interface DoctorRepository extends JpaRepository<Doctor, Long> {

}
