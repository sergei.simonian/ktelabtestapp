package com.example.healthcare.soap;

import com.example.healthcare.service.TimetableService;
import io.spring.testapp.AddTimetableRequest;
import io.spring.testapp.AddTimetableResponse;
import io.spring.testapp.Status;
import lombok.RequiredArgsConstructor;
import org.springframework.ws.server.endpoint.annotation.Endpoint;
import org.springframework.ws.server.endpoint.annotation.PayloadRoot;
import org.springframework.ws.server.endpoint.annotation.RequestPayload;
import org.springframework.ws.server.endpoint.annotation.ResponsePayload;

@Endpoint
@RequiredArgsConstructor
public class TimetableEndpoint {

    private static final String NAMESPACE_URI = "http://spring.io/testapp";
    private final TimetableService timetableService;

    @PayloadRoot(namespace = NAMESPACE_URI, localPart = "addTimetableRequest")
    @ResponsePayload
    public AddTimetableResponse addTimetable(@RequestPayload AddTimetableRequest request) throws Exception {
        timetableService.generateAndSaveTickets(request);
        AddTimetableResponse response = new AddTimetableResponse();
        response.setResponse(Status.SUCCESS);
        return response;
    }

}
