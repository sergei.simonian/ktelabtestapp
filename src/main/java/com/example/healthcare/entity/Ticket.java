package com.example.healthcare.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;

import java.sql.Date;
import java.sql.Time;
import java.util.UUID;

@Entity
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Builder
@Table(name = "tickets")
public class Ticket {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Column
    private UUID uuid;
    private Date date;
    private Time beginTime;
    private Time endTime;
    @ManyToOne(fetch = FetchType.EAGER, cascade = CascadeType.MERGE)
    @JoinTable(name = "tickets_doctors",
            joinColumns = @JoinColumn(name = "tickets_id", referencedColumnName = "id"),
            inverseJoinColumns = @JoinColumn(name = "doctors_id", referencedColumnName = "id"))
    private Doctor doctor;
    @ManyToOne(fetch = FetchType.EAGER, cascade = CascadeType.MERGE)
    @JoinTable(name = "tickets_patients",
            joinColumns = @JoinColumn(name = "tickets_id", referencedColumnName = "id"),
            inverseJoinColumns = @JoinColumn(name = "patients_id", referencedColumnName = "id"))
    private Patient patient;
    private Boolean available;

    @PrePersist
    public void initializeUUID() {
        if (uuid == null) {
            uuid = UUID.randomUUID();
        }
    }

}
