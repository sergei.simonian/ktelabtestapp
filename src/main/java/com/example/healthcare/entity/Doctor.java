package com.example.healthcare.entity;

import javax.persistence.*;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.sql.Date;
import java.util.List;
import java.util.UUID;

@Entity
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Builder
@Table(name = "doctors")
public class Doctor {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Column
    private UUID uuid;
    private String specialization;
    private String firstName;
    private String middleName;
    private String lastName;
    private Date birthDate;
    @OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.MERGE)
    @JoinTable(name = "tickets_doctors",
            joinColumns = @JoinColumn(name = "doctors_id", referencedColumnName = "id"),
            inverseJoinColumns = @JoinColumn(name = "tickets_id", referencedColumnName = "id"))
    private List<Ticket> tickets;

    @PrePersist
    public void initializeUUID() {
        if (uuid == null) {
            uuid = UUID.randomUUID();
        }
    }

}
