package com.example.healthcare.entity;

import javax.persistence.*;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.sql.Date;
import java.util.List;
import java.util.UUID;

@Entity
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Builder
@Table(name = "patients")
public class Patient {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    @Column
    private UUID uuid;
    private String firstName;
    private String middleName;
    private String lastName;
    private Date birthDate;
    @OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.MERGE)
    @JoinTable(name = "tickets_patients",
            joinColumns = @JoinColumn(name = "patients_id", referencedColumnName = "id"),
            inverseJoinColumns = @JoinColumn(name = "tickets_id", referencedColumnName = "id"))
    private List<Ticket> tickets;

}
