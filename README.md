В SOAP и Rest используется кодогенерация

В Rest OpenAPI и swagger codegen.

Rest Тестировал с помощью Postman

Для начала можно зайти в SoapUI и создать расписание

http://localhost:8080/ws/timetable.wsdl


<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:tes="http://spring.io/testapp">
   <soapenv:Header/>
   <soapenv:Body>
      <tes:addTimetableRequest>
         <tes:date>2022-02-02</tes:date>
         <tes:beginTime>09:00:00</tes:beginTime>
         <tes:endTime>18:00:00</tes:endTime>
         <tes:ticketDuration>PT15M</tes:ticketDuration>
         <tes:doctorId>1</tes:doctorId>
      </tes:addTimetableRequest>
   </soapenv:Body>
</soapenv:Envelope>

----------------------------------------------------------------------------------------------------------
Получаем свободные талоны

POST localhost:8080/freetickets

{
    "doctor": 1,
    "date": "2022-02-02"
}

----------------------------------------------------------------------------------------------------------
Выбираем талон и назначаем пациента

POST localhost:8080/appointment

{
    "doctor": 1,
    "patient": 1,
    "ticket": 3
}

----------------------------------------------------------------------------------------------------------
Все записи пациента по id

GET  localhost:8080/patienttickets/1

БД можно запустить с помощью docker-compose